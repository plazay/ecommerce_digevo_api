const productMocks = [
    {
        id: '1',
        image: '/static/images/camiseta.png',
        title: 'Camiseta',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
      {
        id: '2',
        image: '/static/images/hoodie.png',
        title: 'Hoodie',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
      {
        id: '3',
        image: '/static/images/mug.png',
        title: 'Mug',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
      {
        id: '4',
        image: '/static/images/pin.png',
        title: 'Pin',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
      {
        id: '5',
        image: '/static/images/stickers1.png',
        title: 'Stickers',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
      {
        id: '6',
        image: '/static/images/stickers2.png',
        title: 'Stickers',
        price: 80000,
        description: 'bla bla bla bla bla'
      },
]

module.exports = productMocks;